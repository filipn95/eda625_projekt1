import java.util.Scanner;
import java.util.Random;
import java.math.BigInteger;

public class RabinMiller {
	
	public static void main(String[] args) {
		
		RabinMiller rm = new RabinMiller();
//		Gör 20 st anrop till checkprime mellan 2-20
		rm.checkPrime(17);
	}
	
	public boolean checkPrime(int num) {
		if(num <= 3) {
			return false;
		}
		
		Random rand = new Random();
		int randVal = rand.nextInt(num-2) + 2;
		
		int s = num - 1;
        while (s % 2 == 0) {
            s /= 2;
        }
        
        int x = expMod(randVal, s, num);
        if(x == 1 || x == (num-1)) {
        	return true;
        }
        for(int j = 1; j < r-1)
		return true;
	}
	
	public int expMod(int nbr, int exp, int mod) {
		if(nbr == 0) {
			return 0;
		}
		if(exp == 0) {
			return 1;
		}
		if(isOdd(exp)) {
			return (nbr * expMod(nbr, exp-1, mod)) % mod; 
		} else {
			return (int) (Math.pow(expMod(nbr, (exp/2), mod), 2) % mod);
		}
	}
	
	public boolean isOdd(int x) {
		if(x % 2 == 0) {
			return false;
		}
		return true;
	}
}