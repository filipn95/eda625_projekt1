import static org.junit.Assert.*;

import org.junit.Test;

public class TestRabinMiller {
	RabinMiller rb = new RabinMiller();
	
	@Test
	public void testCheckPrime() {
		
		assertFalse(rb.checkPrime(16));
		assertTrue(rb.checkPrime(17));
	}
	
	@Test
	public void textIsOdd() {
		
		assertTrue(rb.isOdd(3));
		assertFalse(rb.isOdd(4));
	}
	
	@Test
	public void testExpMod() {
		
		assertEquals(0, rb.expMod(4, 6, 2));
		assertEquals(1, rb.expMod(4, 10, 3));
		assertNotEquals(0, rb.expMod(4, 10, 3));
	}

}
